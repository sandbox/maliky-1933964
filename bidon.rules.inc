<?php

/**
 * @file
 * C'est un fichier qui sera chargé que lorsque rules en aura besoin
 */

/**
 * un hook qui offre à rules une nouvelle condition basée sur notre module
 * nous voulons permettre la reconnaissance d'un text qui contient une liste de mots bidons
 * @param => <  >
 */
function bidon_rules_condition_info() {
  $conditions = array(
    'bidon_cond_bidon_alo' => array(
      'label' => t('A condition de trouver des bidons'),
      'parameter' => array(
        'leau' => array(
          'label' => t('l\'eau à chercher'),
          'type' => 'text',
        ),
        'lesbidons' => array(
          'label' => t('les bidons à l\'eau'),
          'type' => 'list<text>',
        ),
      ),
      'group' => t('les bidons'),
    ),
  );
  return $conditions;
}

/**
 * verifie qu'au moins un bidon est à l'eau retourne vrai si c'est le cas.
 * @param
 * $leau: où l'on doit chercher les bidons
 * $listebidons: les bidons que l'on doit chercher
 */
function bidon_cond_bidon_alo($leau, $listebidons) {
  // loop sur les bidons et regarde si ils sont à l'eau. Vrai si oui
  foreach ($listebidons as $bidon) {
    if (stristr($leau, $bidon)) {
      return TRUE;
    }
  }

  // si aucun bidon est à l'eau retourn Faux.
  return FALSE;
}