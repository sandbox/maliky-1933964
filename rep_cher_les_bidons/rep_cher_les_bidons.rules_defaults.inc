<?php
/**
 * @file
 * rep_cher_les_bidons.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function rep_cher_les_bidons_default_rules_configuration() {
  $items = array();
  $items['rules_bidon_leau'] = entity_import('rules_config', '{ "rules_bidon_leau" : {
      "LABEL" : "bidon\\u00e0 leau",
      "PLUGIN" : "rule",
      "TAGS" : [ "bidon" ],
      "REQUIRES" : [ "bidon", "rules" ],
      "IF" : [
        { "bidon_cond_bidon_alo" : {
            "leau" : "Il \\u00e9tait une fois un petit bonhomme.",
            "lesbidons" : [ "site:liste-bidons" ]
          }
        }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "l\\u0027un des bidons \\u00e0 \\u00e9t\\u00e9 retrouv\\u00e9 \\u00e0 l\\u0027eau, bravo" } },
        { "LOOP" : {
            "USING" : { "list" : [ "site:liste-bidons" ] },
            "ITEM" : { "bidon" : "un bidon" },
            "DO" : [ { "drupal_message" : { "message" : [ "bidon" ] } } ]
          }
        }
      ]
    }
  }');
  return $items;
}
